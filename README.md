# run-dry

Using oil delivery records and weather, run-dry estimates amount
of oil remaining in the tank and when the tank will run dry.

## Setup

Run-dry needs pandas, scikit learn and other python packages. To install them, do:

        pip install -r requirements.txt
	
## Using run-dry

For help:

        python run-dry.py -h

In addition to specifying options on the command line, you can set the
RUN_DRY_CMD_LINE environment variable to pass all the options and
invoke with:

        python run-dry.py

Sample output:

```
rscohn1@rscohn1-MOBL:/mnt/c/Users/rscohn1/local/Projects/ipin/run-dry$ head OilDelivery.csv
Date,Gallons
11/25/2017,161.5
1/1/2018,230.8
1/25/2018,157.3
2/26/2018,159.6
4/9/2018,157.6
8/29/2018,107.3
11/20/2018,160.4
12/18/2018,156
1/16/2019,171.7
run-dry$ python run-dry.py --capacity 245 --indoor-temp 60 --latlong 42.769506,-71.276138 --darksky-key MYDARKSKYKEY
run-dry.py: Getting timezone
run-dry.py: Reading deliveries
run-dry.py: Getting weather
run-dry.py: Extracting features
run-dry.py: Forecast weather
run-dry.py: Predicting usage
Tank will run dry in 5 days 11 hours on Dec 15 2019
Estimated fill: 214
Estimated remaining: 30
run-dry$ 
```

The estimate uses oil delivery history, indoor temperature, and
historical and forecast weather conditions. The user provides location
of the building, average indoor temperature, and oil delivery
records. Run-dry uses the location of the building to download weather
data from [Dark Sky](https://darksky.net/dev). Run dry uses linear
regression to predict the oil consumed since the last fill. It then
prints the estimates and can optionally send you a text with the same
information.

To use Run-dry, you will need a [Dark Sky](https://darksky.net/dev)
key. If you want to receive texts, you need a free
[Twilio](https://www.twilio.com/) account.

Use [Google
Maps](https://support.google.com/maps/answer/18539?co=GENIE.Platform%3DDesktop&hl=en)
to find the latitude and longitude of the building. You also need
information from past oil deliveries--the date delivered and number of
gallons. See above for the format. Run-dry assumes that each delivery
fills the tank, making it possible to know how much oil is used since
the last fill, which can be combined with the temperature for
predictions. If you only do partial fills then the prediction will be
less accurate. Run-dry uses a single indoor temperature. As long as
there are not drastic changes in the indoor temperature, picking a
single average number should be sufficient.

## Metric vs Imperial

I have only tested with Imperial measures (gallons/Fahrenheit), but
run-dry should work equally well for metric. Dark Sky data is
configured to use units appropriate for the geography of the target
location so set your --indoor-temp as appropriate. You should also
ensure the units used in OilDelivery.csv and -capacity are
consistent. The code uses the work gallons internally, but that does
not affect the user interface.

## Time zones

Run-dry was tested when it is run in a different time zone than the
target. This is important when you run it in AWS or other cloud
service.
