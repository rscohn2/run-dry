import argparse
import csv
import datetime
import json
import os
import requests
import statistics
import sys
import time

import numpy as np
import pandas as pd
import pytz
from sklearn.linear_model import LinearRegression
from twilio.rest import Client as TwilioClient

def error(message):
    sys.exit('%s: %s' % (os.path.basename(__file__),message))
    
def warning(message):
    print('%s: %s' % (os.path.basename(__file__),message), file=sys.stderr)
    
def log(verbose, message):
    if verbose:
        print('%s: %s' % (os.path.basename(__file__),message), file=sys.stderr)
    
# Weather from Darksky, with persistent caching
# Darksky interface
#   persistent cache of historical data
#     dictionary while running, file between runs
#     context manager events trigger reading,writing the file
#   computes datetime based on timestamp & timezone in response
class DarkSky(object):
    def __init__(self, darksky_key, latlong, weather_file='weather-cache.json', verbose=False):
        self.verbose = verbose
        self.weather_file = weather_file
        self.latlong = latlong
        self.url_base = 'https://api.darksky.net/forecast/%s/%s' % (darksky_key,latlong)
        current_url = '%s?extend=hourly&units=auto' % self.url_base
        self.current = json.loads(requests.get(current_url).text)
        self.tz = pytz.timezone(self.current['timezone'])
        try:
            with open(weather_file,'r') as fin:
                self.cache = json.load(fin)
        except:
            if weather_file:
                warning(args, 'Error reading weather cache file: %s' % weather_file)
            self.cache = {}
            
    def __enter__(self):
        return self
    
    def __exit__(self, type, value, traceback):
        try:
            with open(self.weather_file,'w') as fout:
                json.dump(self.cache, fout, indent=4)
        except:
            if self.weather_file:
                warning(args, 'Error writing weather cache file: %s' % weather_file)

        
    def dt(self, o):
        return datetime.datetime.fromtimestamp(o['time'], tz=self.tz)
    
    def day(self, dt):
        ts = str(int(dt.timestamp()))
        if ts in self.cache:
            return self.cache[ts]
        url = '%s,%s?units=auto' % (self.url_base,ts)
        log(self.verbose, 'requesting: %s' % url)
        self.cache[ts] = json.loads(requests.get(url).text)
        return self.cache[ts]

# Read oil delivery data from a csv file
def get_deliveries(args, timezone):
    try:
      deliveries = pd.read_csv(args.deliveries).drop_duplicates()
      deliveries.Date = pd.to_datetime(deliveries.Date).dt.tz_localize(timezone)
      return deliveries
    except:
      error('Error reading oil delivery data from ' + args.deliveries)

def localize_series(series, timezone):
    return pd.to_datetime(series, unit='s').dt.tz_localize('UTC').dt.tz_convert(timezone)
    
def get_timezone(args):
    # darksky will convert latlong to timezone
    with DarkSky(args.darksky_key, args.latlong, weather_file=None, verbose=args.verbose) as ds:
        return ds.tz
    
# Get weather information for the dates covered by the delivery information
def get_weather(args, deliveries):
    with DarkSky(args.darksky_key, args.latlong, verbose=args.verbose) as ds:
        # Get historical data
        hours = []
        date = deliveries.Date.iloc[0]
        end_date = ds.dt(ds.current['hourly']['data'][0])
        while date < end_date:
            day = ds.day(date)
            hours.extend(day['hourly']['data'])
            date = date + datetime.timedelta(days=1)
        h = pd.DataFrame(hours)
        ind = localize_series(h.time, ds.tz)
        h = h.set_index(ind).sort_index().asfreq('H').fillna(method='ffill')
        return ds.current,h

# Extract features from weather and delivery information
def HDH(row, weather, indoor_temp):
    temps = weather['temperature'].loc[row.BeginDate:row.EndDate]
    return np.maximum(0,indoor_temp-temps).mean()

def extract_features(args, deliveries, weather):
    features = pd.DataFrame(columns=['DeliveryDate','Gallons','BeginDate','EndDate','Hours','HDH','GPH'])
    features.DeliveryDate = deliveries.Date[1:]
    features.Gallons = deliveries.Gallons[1:]
    # assume the delivery occured at midnight
    features.BeginDate = deliveries.Date.shift(1)
    features.EndDate = deliveries.Date - datetime.timedelta(minutes=1)
    features.Hours = (features.EndDate - features.BeginDate).dt.total_seconds()/60/60
    # Compute the actual features
    features.GPH = features.Gallons / features.Hours
    features.HDH = features.apply(lambda row : HDH(row, weather, args.indoor_temp) , axis=1)
    return features

# Build a model based on features
def build_model(features):
    # Use HDH to predict GPH
    hdh = features.HDH.values.reshape(-1,1)
    gph = features.GPH.values.reshape(-1,1)
    return LinearRegression().fit(hdh,gph)

# Generate a forecast
def forecast_weather(args, timezone, historical, current):
    # Hourly data for at least 48 hours
    hourly = pd.DataFrame(current['hourly']['data'])
    
    # Daily has min/max temperature and time for each day for 7 days
    daily = pd.DataFrame(current['daily']['data'])

    # Break min & max temp into separate dataframes
    daily_min = pd.DataFrame({'time': daily.temperatureMinTime, 'temperature': daily.temperatureMin})
    daily_max = pd.DataFrame({'time': daily.temperatureMaxTime, 'temperature': daily.temperatureMax})
    daily_mean = statistics.mean([daily.temperatureMin.mean(),daily.temperatureMax.mean()])
    max_historical = historical['time'].max()
    max_hourly = hourly['time'].max()
    rest_begin = max([max_historical,
                      max_hourly,
                      daily.temperatureMinTime.max(),
                      daily.temperatureMaxTime.max()]) + 12*60*60
    rest_end = rest_begin + args.forecast_days*24*60*60
    rest = pd.DataFrame({'time': [rest_begin,rest_end], 'temperature': [daily_mean, daily_mean]})
    f = pd.concat([hourly[hourly.time > max_historical],
                   daily_min[daily_min.time > max_hourly],
                   daily_max[daily_max.time > max_hourly],
                   rest],
                  sort=False).drop_duplicates(subset='time')
    f.time = localize_series(f.time, timezone)
    f = f.set_index('time').sort_index().asfreq('H').fillna(method='ffill')
    return f

def get_today(timezone):
    return pd.to_datetime('today').tz_localize(timezone)

# Predict usage from last delivery to 30 days in the future
def predict_usage(args, model, deliveries, weather, timezone):
    one_day = datetime.timedelta(days=1)
    one_minute = datetime.timedelta(minutes=1)
    date = deliveries['Date'].iloc[-1]
    end_date = get_today(timezone) + args.forecast_days * one_day
    intervals = []
    while date < end_date:
        intervals += [{'BeginDate': date,
                        'EndDate': date + one_day - one_minute,
                        'Hours': None,
                        'HDH': None,
                        'GPH': None,
                        'Gallons': None,
                        'CumGallons': None}]
        date += one_day
    prediction = pd.DataFrame(intervals)
    prediction.Hours = (prediction.EndDate - prediction.BeginDate).dt.total_seconds()/60/60
    prediction.HDH = prediction.apply(lambda row : HDH(row,weather, args.indoor_temp), axis=1)
    prediction.GPH = model.predict(prediction['HDH'].values.reshape(-1,1)).flatten()
    prediction.Gallons = prediction.Hours * prediction.GPH
    prediction.CumGallons = prediction.Gallons.cumsum()
    return prediction

def estimate_fill(prediction, today):
    fill = prediction[today >= prediction.EndDate].iloc[-1].CumGallons
    partial = prediction[today < prediction.EndDate].iloc[0]
    if not partial.empty:
        fill += partial.GPH * (today - partial.BeginDate).total_seconds() / (60*60)
    return fill
    
def message_data(args, prediction, timezone):
    today = get_today(timezone)
    fill = estimate_fill(prediction, today)

    dry_dates = prediction[prediction.CumGallons > args.capacity]
    if len(dry_dates.index) == 0:
        return fill,'Tank will not run dry in next %d days' % args.forecast_days
    else:
        dry_date = dry_dates.iloc[0].BeginDate
        dry_string = dry_date.strftime('%b %d %Y')
        if today >= dry_date:
            return args.capacity,'Tank ran dry on %s' % dry_string
        else:
            delta = dry_date - today
            hours = delta.seconds//(60*60)
            message = 'Tank will run dry in %d day%s %d hour%s on %s' % (
                delta.days,
                's' if delta.days > 1 else '',
                hours,
                's' if hours > 1 else '',
                dry_string)
            return fill,message
    
# Use predicted usage to determine when tank will run dry
def emit_run_dry(args, prediction, timezone):
    fill,message = message_data(args, prediction, timezone)
    message += '\nEstimated fill: %d' % fill
    message += '\nEstimated remaining: %d' % (args.capacity - fill)
    print(message)
    if args.twilio_sid:
        TwilioClient(args.twilio_sid, args.twilio_token).messages.create(
            body='|\n\n' + message,
            from_=args.twilio_from,
            to=args.twilio_to
        )

def run_dry(args):
    verbose = args.verbose
    log(verbose, 'Getting timezone')
    timezone = get_timezone(args)
    log(verbose, 'Reading deliveries')
    deliveries = get_deliveries(args, timezone)
    log(verbose, 'Getting weather')
    current_weather, historical = get_weather(args, deliveries)
    log(verbose, 'Extracting features')
    features = extract_features(args, deliveries, historical)
    model = build_model(features)
    log(verbose, 'Forecast weather')
    forecast = forecast_weather(args, timezone, historical, current_weather)
    historical_and_forecast = pd.concat([historical,forecast], sort=True)
    log(verbose, 'Predicting usage')
    prediction = predict_usage(args, model, deliveries, historical_and_forecast, timezone)
    emit_run_dry(args, prediction, timezone)

def main():
    parser = argparse.ArgumentParser(
        description='Calculate when a heating oil tank will run dry.')
    default_string = '(default: %(default)s)'
    parser.add_argument('--capacity',
                        type=int,
                        required=True,
                        help='capacity of the oil tank. Example 245')
    parser.add_argument('--darksky-key',
                        required=True,
                        help='API key for darksky weather service')
    parser.add_argument('--deliveries',
                        default='OilDelivery.csv',
                        help='delivery dates and amounts as a CSV %s' % default_string)
    parser.add_argument('--forecast-days',
                        type=int,
                        default=30,
                        help='number of days to forecast %s' % default_string)
    parser.add_argument('--indoor-temp',
                        type=int,
                        required=True,
                        help='indoor temperature. Example 60')
    parser.add_argument('--latlong',
                        required=True,
                        help='latitude and longitude for the calculation. Example 42.769506,-71.276138')
    parser.add_argument('--twilio-sid',
                        help='Twilio Account SID')
    parser.add_argument('--twilio-token',
                        help='Twilio auth token')
    parser.add_argument('--twilio-from',
                        help='number that sends text')
    parser.add_argument('--twilio-to',
                        help='text sent to this number')
    parser.add_argument('--verbose',
                        action='store_true',
                        help='print informational messages %s' % default_string)
    a = os.environ['RUN_DRY_CMD_LINE'].split() if 'RUN_DRY_CMD_LINE' in os.environ else None
    args = parser.parse_args(args=a)
    run_dry(args)

main()
